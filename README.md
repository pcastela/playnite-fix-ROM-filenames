# playnite-fix-ROM-filenames
Extension for [Playnite](http://playnite.link/) to rename ROM files to the name of the game as displayed on Playnite.

| Before | After |
| --- | --- |
|![Before](images/01_before.png)|![After](images/02_after.png)|

## General info
1. Update your Games' Metadata with correct Name and information. 
1. Select one or more ROMs Games and trigger the script from the menu `Playnite -> Settings -> Extensions -> Rename Selected ROMs`

## Screenshots
![Menu](images/extension_menu.png)

## Setup

Copy the `Fix-ROM-filenames` directory into Playnite's `Extensions` directory. Location of `Extensions` directory differs based on Playnite's installation type:
* Portable version: `Extensions` folder directly inside of Playnite's installation location.
* Installed version: `%AppData%\Playnite\Extensions` folder.

Make sure the extension is enabled in `Playnite -> Settings -> Extensions`.

## Status
Project is: _finished_

## Inspiration
Project inspired by ![Josef Nemec](https://github.com/JosefNemec).
Thanks to darklinkpower for tips on how to improve this extension. 


