using namespace System.IO
using namespace System.Windows

function global:GetMainMenuItems()
{
    param($menuArgs)

    $menuItem = New-Object Playnite.SDK.Plugins.ScriptMainMenuItem
    $menuItem.Description = "Rename Selected ROMs"
    $menuItem.FunctionName = "RenameSelectedRoms"
    $menuItem.MenuSection = "@"
    return $menuItem
}

function global:RenameSelectedRoms()
{
 	$i = 0
	$f = 0
	
	## go to each game in the new selection
	foreach ($game in $PlayniteApi.MainView.SelectedGames) {
		
		## full path of file
		$path = $game.GameImagePath
		
		## name of game
		$name = $game.Name
		
		## extension of file
		$extension = [IO.Path]::GetExtension($path)
		
		## prepare new filename
		$newFileName = ($name.Split([IO.Path]::GetInvalidFileNameChars()) -join '') + $extension
		
		## prepare new image path 
		$newGameImagePath = $game.InstallDirectory + "\" + $newFileName;
		
		## check if new filename and path already exist
		if ([IO.File]::Exists($newGameImagePath))
			{
				$PlayniteApi.Dialogs.ShowMessage("Cannot rename ""$path"" because file ""$newGameImagePath"" already exists", "Error renaming ""$name"" ", 0, 16)
				
				$f++
			}
		else 
			{
				
				## rename file	
				Rename-Item -LiteralPath $path -NewName $newFileName
				
				## update image path accordingly
				$game.GameImagePath = $game.InstallDirectory + "\" + $newFileName;
				
				$i++
			}

	}

	## tell user what happened
	if ($f -eq 0)
		{
			$PlayniteApi.Dialogs.ShowMessage("$i games renamed.", "Success", 0, 64)
		}
	else 
		{
			$PlayniteApi.Dialogs.ShowMessage("$i games renamed and $f failures!", "Warning", 0, 48)
		}
}
